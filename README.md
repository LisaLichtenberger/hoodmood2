# hoodmood2

Check the mood in your hood! 

With hoodmood2, a group of people can gather ideas for how to spending their spare time. For this project, we got a similar idea as Bring!
So we want to build an application in which you can enter a group and share ideas. For example, the group can have a topic "movies" and people
who are in this group can share a list of movies they want to watch together or games they want to play. The group members also can rate the ideas
to find out which movie they want to see first.

Team:
Lukas Ameisbichler,
Lisa Lichtenberger
